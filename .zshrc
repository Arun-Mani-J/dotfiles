# Enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# History
setopt histignorealldups sharehistory
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=$HOME/.cache/zsh/history

# Extend PATH
export PATH=$HOME/.local/bin:$PATH

# Use modern completion system
autoload -Uz compinit
compinit

# Set title
autoload -Uz add-zsh-hook

function xterm_title_precmd () {
	print -Pn -- '\e]2;%~\005\e\'
}

function xterm_title_preexec () {
	print -Pn -- '\e]2;%~ : ' && print -n -- "${(q)1}\a"
}

add-zsh-hook -Uz precmd xterm_title_precmd
add-zsh-hook -Uz preexec xterm_title_preexec

# Start Starship
eval "$(starship init zsh)"

# Plugins
ZSH_AUTOSUGGEST_STRATEGY=(history completion)

source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Environment Variables
export HTML_TIDY="$HOME/.config/tidy/tidyrc"

if [ -e /home/arun-mani-j/.nix-profile/etc/profile.d/nix.sh ]; then . /home/arun-mani-j/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
